### tomcat6-gitlab-openshift [![build status](https://gitlab.com/developer.cyrus/tomcat6-gitlab-openshift/badges/master/build.svg)](https://gitlab.com/developer.cyrus/tomcat6-gitlab-openshift/commits/master) [![coverage report](https://gitlab.com/developer.cyrus/tomcat6-gitlab-openshift/badges/master/coverage.svg)](https://gitlab.com/developer.cyrus/tomcat6-gitlab-openshift/commits/master)

Some important notes:
1. This is a demo for Continuous Integration by Gitlab CI to deploy a sample Tomcat webapp to Openshift by uploading war file by scp in gitlab-runner of docker machine. 
2. The webapp is located in [http://jbossews-webservicesbeam.rhcloud.com/tomcat6-gitlab-openshift](http://jbossews-webservicesbeam.rhcloud.com/tomcat6-gitlab-openshift)
3. The 2nd step (test) in .gitlab-ci.yml requires to use jacoco-maven-plugin, and also requires to paste the value "Total.*?([0-9]{1,3})%" in Setting >  CI/CD Pipelines > Test coverage parsing in gitlab.
4. The last step (deploy) in .gitlab-ci.yml involves the public key and private key. Both are generated by puttygen.exe. The public key is required to paste in openshift, while the private key is required to store as $SSH\_PRIVATE\_KEY in Setting > CI/CD Pipelines > Secret Variables in gitlab. The variable will then be used in runtime during scp upload.
 


