package foo.bar.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ConverterTest {
	
	@Test
	public void testAppend() {
		String actual = Converter.append("Hello World");
		String expected = "Hello World!";
		
		assertEquals(expected, actual);
	}
}
